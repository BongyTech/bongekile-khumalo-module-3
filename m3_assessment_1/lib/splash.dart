import 'dart:async';

import 'package:flutter/material.dart';

import 'login.dart';

void main() {
  runApp(SplashScreen());
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StartState();
}

class StartState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    startTime();
  }

  startTime() async {
    var duration = Duration(seconds: 4);
    return new Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(context, MaterialPageRoute(
      builder: (context) => LoginScreen()
    ));
  }

  @override
  Widget build(BuildContext context) {
    return initWidget(context);
  }

  Widget initWidget(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Color.fromARGB(255, 31, 220, 245),
             gradient: LinearGradient(colors: [(new  Color.fromARGB(255, 5, 196, 180)), Color.fromARGB(255, 97, 236, 225)],
                      begin: Alignment.centerLeft,
                      end: Alignment.centerRight
                )
            ),
          ),
          Center(
            child: Container(
              child: Image.asset("images/logo.png"),
            ),
          )
        ],
      ),
    );
  }
}
